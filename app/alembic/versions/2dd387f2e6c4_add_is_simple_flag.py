"""add is_simple_flag

Revision ID: 2dd387f2e6c4
Revises: 689d19d5b7ae
Create Date: 2023-09-15 14:51:27.504997

"""
from typing import Sequence, Union

import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision: str = "2dd387f2e6c4"
down_revision: Union[str, None] = "689d19d5b7ae"
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column("recipe", sa.Column("is_simple", sa.Boolean(), server_default=sa.text("false"), nullable=False))
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("recipe", "is_simple")
    # ### end Alembic commands ###
