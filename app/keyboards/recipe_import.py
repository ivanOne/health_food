from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.utils.keyboard import InlineKeyboardBuilder

from dto.recipe_import import RecipeTypeSelectCallback
from models.recipe import RecipeType


def recipe_type_choice_menu() -> InlineKeyboardMarkup:
    builder = InlineKeyboardBuilder()
    for recipe_type in RecipeType:
        builder.row(
            InlineKeyboardButton(
                text=recipe_type.value, callback_data=RecipeTypeSelectCallback(recipe_type=recipe_type).pack()
            )
        )
    return builder.as_markup()
