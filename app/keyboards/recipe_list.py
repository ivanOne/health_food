from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.utils.keyboard import InlineKeyboardBuilder

from dto.recipe_view import RecipeList, RecipeListCallback, RecipeListView
from models.recipe import RecipeType


def recipe_list_menu(data: RecipeList) -> InlineKeyboardMarkup:
    builder = InlineKeyboardBuilder()

    for recipe in data.data:
        builder.row(
            InlineKeyboardButton(
                text=recipe.title,
                callback_data=RecipeListCallback(action=RecipeListView.DETAIL, recipe_id=recipe.id).pack(),
            )
        )

    if data.filters.recipe_type is None:
        builder.row(
            InlineKeyboardButton(
                text="Отфильтровать по типу рецепта",
                callback_data=RecipeListCallback(action=RecipeListView.SELECT_FILTER).pack(),
            )
        )
    else:
        builder.row(
            InlineKeyboardButton(
                text=f"Отфильтровано ({RecipeType(data.filters.recipe_type).value})",
                callback_data=RecipeListCallback(
                    action=RecipeListView.SELECT_FILTER, selected_filter=RecipeType(data.filters.recipe_type)
                ).pack(),
            )
        )
    return builder.as_markup()


def recipe_type_choice_menu(selected_recipe_type: RecipeType | None = None) -> InlineKeyboardMarkup:
    builder = InlineKeyboardBuilder()
    for recipe_type in RecipeType:
        builder.row(
            InlineKeyboardButton(
                text=f"{recipe_type.value} (Выбрано)" if recipe_type == selected_recipe_type else recipe_type.value,
                callback_data=RecipeListCallback(action=RecipeListView.LIST_FILTER, selected_filter=recipe_type).pack(),
            )
        )
    builder.row(InlineKeyboardButton(text="Все", callback_data=RecipeListCallback(action=RecipeListView.LIST).pack()))
    return builder.as_markup()
