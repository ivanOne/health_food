import asyncio
import logging
import sys

from aiogram import Bot, Dispatcher
from aiogram.fsm.storage.redis import RedisStorage
from aiogram.types import BotCommand
from redis.asyncio.client import Redis

from config import app_config
from handlers import main, recipe_importer, recipe_list, settings


async def setup_bot_commands(bot: Bot) -> None:
    bot_commands = [
        BotCommand(command="/cancel", description="Отмена действий"),
        BotCommand(command="/recipe_list", description="Список рецептов"),
        BotCommand(command="/set_persons", description="Установить количество человек"),
    ]
    await bot.set_my_commands(bot_commands)


async def run_poll() -> None:
    bot = Bot(token=app_config.admin_bot_token)
    await setup_bot_commands(bot)
    dispatcher = Dispatcher(bot=bot, storage=RedisStorage(redis=Redis.from_url(app_config.redis_url)))
    routers = [main, recipe_list, recipe_importer, settings]
    for router in routers:
        dispatcher.include_router(router.router)
    await dispatcher.start_polling(bot)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    asyncio.run(run_poll())
