from sqlalchemy import select
from sqlalchemy.exc import NoResultFound

from crud.base import CRUDBase
from models.recipe import MeasurementUnit, Product


class ProductCRUD(CRUDBase):
    async def get_or_create(self, title: str, measurement_unit: MeasurementUnit) -> Product:
        try:
            query = select(Product).where(Product.title == title)
            return (await self.session.execute(query)).scalar_one()
        except NoResultFound:
            return await self.create(title=title, measurement_unit=measurement_unit)

    async def create(self, title: str, measurement_unit: MeasurementUnit) -> Product:
        product = Product(title=title, measurement_unit=measurement_unit)
        self.session.add(product)
        await self.session.commit()
        await self.session.refresh(product)
        return product
