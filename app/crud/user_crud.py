from sqlalchemy import select

from crud.base import CRUDBase
from models.user import User


class UserCRUD(CRUDBase):
    async def create(self, user: User) -> User:
        self.session.add(user)
        await self.session.commit()
        await self.session.refresh(user)
        return user

    async def get_by_telegram_id(self, telegram_id: int) -> User:
        query = select(User).where(User.telegram_user_id == telegram_id)
        return (await self.session.execute(query)).scalar_one()

    async def update(self, user: User, person_count: int) -> None:
        user.person_count = person_count
        await self.session.commit()
