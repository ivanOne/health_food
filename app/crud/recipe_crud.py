from typing import Sequence
from uuid import UUID

from sqlalchemy import select
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import selectinload

from crud.base import CRUDBase
from models.recipe import Product, Recipe, RecipeProduct, RecipeType


class RecipeCRUD(CRUDBase):
    async def get_or_create(self, **kwargs) -> Recipe:
        try:
            recipe = (await self.session.execute(select(Recipe).filter_by(**kwargs))).scalar_one()
            return recipe
        except NoResultFound:
            return await self.create(Recipe(**kwargs))

    async def create(self, recipe: Recipe) -> Recipe:
        self.session.add(recipe)
        await self.session.commit()
        await self.session.refresh(recipe)
        return recipe

    async def add_products(self, recipe: Recipe, products: list[tuple[Product, int]]) -> None:
        recipe_products = [RecipeProduct(recipe=recipe, product=product, count=count) for product, count in products]
        self.session.add_all(recipe_products)
        await self.session.commit()

    async def all(self, recipe_type: RecipeType | None = None) -> Sequence[Recipe]:
        query = select(Recipe)
        if recipe_type:
            query = query.where(Recipe.recipe_type == recipe_type)
        return (await self.session.execute(query)).scalars().all()

    async def get_by_id(self, recipe_id: UUID) -> Recipe:
        query = (
            select(Recipe)
            .where(Recipe.id == recipe_id)
            .options(
                selectinload(Recipe.products).joinedload(RecipeProduct.product),
            )
        )
        return (await self.session.execute(query)).scalar_one()
