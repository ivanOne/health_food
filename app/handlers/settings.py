from aiogram import Router, types
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup

from middleware import UserFilterMiddleware
from models.db import async_session_factory
from services.user_service import UserService

router = Router(name="recipe_list")
router.message.outer_middleware(UserFilterMiddleware())


class PersonCountInput(StatesGroup):
    input_count = State()


@router.message(Command("set_persons"))
async def question_person_count(message: types.Message, state: FSMContext) -> None:
    async with async_session_factory() as session:
        current_person_count = await UserService(session=session).get_person_count(message.from_user.id)
    await state.set_state(PersonCountInput.input_count)
    await message.answer(f"Сколько человек (сейчас указано - {current_person_count})? ввести можно число - 1, 2 или 3")


@router.message(PersonCountInput.input_count)
async def set_person_count(message: types.Message, state: FSMContext) -> None:
    try:
        person_count = int(message.text)
        async with async_session_factory() as session:
            await UserService(session=session).create_or_update_user(message.from_user.id, person_count)
            await state.clear()
            await message.answer("Сохранено")
    except ValueError:
        await message.answer("Нужно ввести число - 1, 2 или 3")
