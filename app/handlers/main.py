import logging

from aiogram import F, Router
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import Message

from middleware import UserFilterMiddleware

router = Router(name="main")
router.message.outer_middleware(UserFilterMiddleware())


@router.message(Command("cancel"))
@router.message(F.text.casefold() == "cancel")
@router.message(F.text.casefold() == "отмена")
async def cancel_handler(message: Message, state: FSMContext) -> None:
    current_state = await state.get_state()
    if current_state is not None:
        logging.info(f"Очистка состояния {current_state}")
        await state.clear()
    await message.answer("Отменено")
