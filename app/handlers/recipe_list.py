import logging

from aiogram import F, Router, types
from aiogram.filters import Command
from aiogram.types import CallbackQuery, FSInputFile

from dto.recipe_view import RecipeListCallback, RecipeListView
from keyboards.recipe_list import recipe_list_menu, recipe_type_choice_menu
from middleware import UserFilterMiddleware
from models.db import async_session_factory
from models.recipe import RecipeType
from services.recipe_view_service import RecipeDetailError, RecipeViewService
from services.user_service import UserService

router = Router(name="recipe_list")
router.message.outer_middleware(UserFilterMiddleware())


async def recipes_list_message(message: types.Message, recipe_type: RecipeType | None = None) -> None:
    async with async_session_factory() as session:
        persons = await UserService(session=session).get_person_count(message.from_user.id)
        data = await RecipeViewService(session=session, recipe_type=recipe_type, person_count=persons).build_list()
    if data.filters.recipe_type is None:
        title = "Список всех рецептов"
    else:
        title = f"Список рецептов для {RecipeType(data.filters.recipe_type).value}а"
    await message.answer(title, reply_markup=recipe_list_menu(data))


@router.message(Command("recipe_list"))
async def recipes_list(message: types.Message) -> None:
    await message.answer("Какие рецепты показать?", reply_markup=recipe_type_choice_menu())


@router.callback_query(RecipeListCallback.filter(F.action == RecipeListView.LIST))
async def filter_recipe_list(callback: CallbackQuery) -> None:
    await recipes_list_message(callback.message)


@router.callback_query(RecipeListCallback.filter(F.action == RecipeListView.LIST_FILTER))
async def filtered_recipe_list(callback: CallbackQuery, callback_data: RecipeListCallback) -> None:
    await recipes_list_message(callback.message, callback_data.selected_filter)


@router.callback_query(RecipeListCallback.filter(F.action == RecipeListView.SELECT_FILTER))
async def select_filter(callback: CallbackQuery, callback_data: RecipeListCallback) -> None:
    await callback.message.answer(
        "Фильтры по типу меню", reply_markup=recipe_type_choice_menu(callback_data.selected_filter)
    )


@router.callback_query(RecipeListCallback.filter(F.action == RecipeListView.DETAIL))
async def recipe_detail(callback: CallbackQuery, callback_data: RecipeListCallback) -> None:
    async with async_session_factory() as session:
        try:
            persons = await UserService(session=session).get_person_count(callback.from_user.id)
            data = await (
                RecipeViewService(session=session, recipe_type=callback_data.recipe_id, person_count=persons)
                .build_detail(callback_data.recipe_id)
            )
            file = FSInputFile(data.image)
            await callback.message.reply_photo(caption=data.text, photo=file)
        except RecipeDetailError:
            logging.error("Recipe detail error", exc_info=True)
            await callback.message.reply("Не удалось получить рецепт")
