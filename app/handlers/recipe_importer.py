from typing import Any
from uuid import UUID, uuid4

from aiogram import Bot, Router, types
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup
from aiogram.types import CallbackQuery

from config import app_config
from dto.recipe_import import (
    ImportedIngredient,
    ImportedNutritionQuality,
    ImportedRecipe,
    RecipeTypeSelectCallback,
)
from keyboards.recipe_import import recipe_type_choice_menu
from middleware import UserFilterMiddleware
from models.db import async_session_factory
from services.recipe_import_service import (
    RecipeImportService,
    parse_nutrition_quality,
    parse_products,
)


class RecipeInput(StatesGroup):
    title = State()
    ingredients = State()
    instructions = State()
    recipe_type = State()
    nutrition_quality = State()
    image = State()


router = Router(name="recipe_import")
router.message.outer_middleware(UserFilterMiddleware())


@router.message(Command("recipe_import"))
async def request_title(message: types.Message, state: FSMContext) -> None:
    await state.set_state(RecipeInput.title)

    await message.answer("Введите название рецепта")


@router.message(RecipeInput.title)
async def input_title(message: types.Message, state: FSMContext) -> None:
    await state.update_data(name=message.text)
    await state.set_state(RecipeInput.ingredients)

    await message.answer("Ингредиенты")


@router.message(RecipeInput.ingredients)
async def input_ingredients(message: types.Message, state: FSMContext) -> None:
    await state.update_data(ingredients=parse_products(message.text))
    await state.set_state(RecipeInput.instructions)

    await message.answer("Теперь сам рецепт")


@router.message(RecipeInput.instructions)
async def input_instructions(message: types.Message, state: FSMContext) -> None:
    await state.update_data(instructions=message.text.replace("\n", " ").replace("  ", " "))
    await state.set_state(RecipeInput.recipe_type)

    await message.answer("Тип рецепта", reply_markup=recipe_type_choice_menu())


@router.callback_query(RecipeTypeSelectCallback.filter())
async def input_recipe_type(
    callback: CallbackQuery, callback_data: RecipeTypeSelectCallback, state: FSMContext
) -> None:
    if await state.get_state() == RecipeInput.recipe_type.state:
        await state.update_data(recipe_type=callback_data.recipe_type.value)
    await state.set_state(RecipeInput.nutrition_quality)

    await callback.message.answer("КБЖУ")


@router.message(RecipeInput.nutrition_quality)
async def input_nutrition_quality(message: types.Message, state: FSMContext) -> None:
    await state.update_data(nutrition_quality=parse_nutrition_quality(message.text))
    await state.set_state(RecipeInput.image)

    await message.answer("Фото")


@router.message(RecipeInput.image)
async def input_recipe_photo(message: types.Message, state: FSMContext, bot: Bot) -> None:
    if photos := message.photo:
        data = await state.get_data()
        uuid = uuid4()

        await save_recipe(state_data=data, recipe_id=uuid)

        file_path = await bot.get_file(photos[-1].file_id)
        await bot.download_file(file_path.file_path, f"{app_config.media_folder}/{uuid}.jpg")
        await message.answer("Рецепт добавлен")
        await state.clear()
    else:
        await message.answer("Нужно отправить фото")


async def save_recipe(state_data: dict[str, Any], recipe_id: UUID) -> None:
    data = ImportedRecipe(
        id=recipe_id,
        name=state_data["name"],
        instructions=state_data["instructions"],
        recipe_type=state_data["recipe_type"],
        nutrition_quality=ImportedNutritionQuality(
            calories=state_data["nutrition_quality"]["calories"],
            proteins=state_data["nutrition_quality"]["proteins"],
            fat=state_data["nutrition_quality"]["fat"],
            carbohydrates=state_data["nutrition_quality"]["carbohydrates"],
        ),
        image=f"{recipe_id}.jpg",
        ingredients=[
            ImportedIngredient(
                name=ingredient["name"], measure_type=ingredient["measure_type"], count=ingredient["count"]
            )
            for ingredient in state_data["ingredients"]
        ],
    )
    async with async_session_factory() as session:
        await RecipeImportService(session).save(data)
