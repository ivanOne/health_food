from typing import Annotated, AsyncGenerator

from sqlalchemy import String
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, create_async_engine
from sqlalchemy.orm import DeclarativeBase, mapped_column

from config import app_config

engine = create_async_engine(app_config.postgres_url, echo=True)
async_session_factory = async_sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)


str_255 = Annotated[str, mapped_column(String(255))]


async def get_session() -> AsyncGenerator:
    async with async_session_factory() as session:
        yield session


class Base(DeclarativeBase):
    pass
