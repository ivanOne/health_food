from sqlalchemy import BigInteger
from sqlalchemy.orm import Mapped, mapped_column

from models.db import Base


class User(Base):
    __tablename__ = "user"

    id: Mapped[int] = mapped_column(primary_key=True)
    telegram_user_id: Mapped[int] = mapped_column(BigInteger)
    person_count: Mapped[int] = mapped_column(server_default="1")
