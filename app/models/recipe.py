import uuid
from enum import Enum
from uuid import UUID

from sqlalchemy import ForeignKey, Text
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.sql import expression

from .db import Base, str_255


class RecipeType(Enum):
    BREAKFAST = "Завтрак"
    SECOND_BREAKFAST = "Перекус"
    LUNCH = "Обед"
    AFTERNOON_TEA = "Полдник"
    DINNER = "Ужин"


class MeasurementUnit(Enum):
    GRAM = "gram"
    MILLILITER = "milliliter"
    PIECE = "piece"
    TO_TASTE = "to_taste"


class Recipe(Base):
    __tablename__ = "recipe"

    id: Mapped[UUID] = mapped_column(primary_key=True, default=uuid.uuid4)
    image: Mapped[str_255]
    title: Mapped[str_255]
    instruction: Mapped[str] = mapped_column(Text)
    recipe_type: Mapped[RecipeType]
    calories: Mapped[int]
    proteins: Mapped[int]
    fat: Mapped[int]
    carbohydrates: Mapped[int]
    is_simple: Mapped[bool] = mapped_column(server_default=expression.false())
    products: Mapped[list["RecipeProduct"]] = relationship(back_populates="recipe")


class Product(Base):
    __tablename__ = "product"

    id: Mapped[int] = mapped_column(primary_key=True)
    title: Mapped[str_255]
    measurement_unit: Mapped[MeasurementUnit]
    recipes: Mapped[list["RecipeProduct"]] = relationship(back_populates="product")


class RecipeProduct(Base):
    __tablename__ = "recipe_product"
    left_id: Mapped[UUID] = mapped_column(ForeignKey("recipe.id"), primary_key=True)
    right_id: Mapped[int] = mapped_column(ForeignKey("product.id"), primary_key=True)
    count: Mapped[int | None]
    recipe: Mapped["Recipe"] = relationship(back_populates="products", lazy=True)
    product: Mapped["Product"] = relationship(back_populates="recipes", lazy=True)
