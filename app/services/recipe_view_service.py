from uuid import UUID

from sqlalchemy.exc import NoResultFound
from sqlalchemy.ext.asyncio import AsyncSession

from config import app_config
from crud.recipe_crud import RecipeCRUD
from dto.recipe_view import RecipeDetail, RecipeList, RecipeListFilters, RecipeListItem
from models.recipe import MeasurementUnit, Recipe, RecipeProduct, RecipeType
from services.base_service import BaseService


class RecipeDetailError(Exception):
    pass


class RecipeViewService(BaseService):
    recipe_type: RecipeType | None
    person_count: int

    def __init__(self, session: AsyncSession, person_count: int, recipe_type: RecipeType | None = None):
        super().__init__(session)
        self.recipe_type = recipe_type
        self.person_count = person_count

    async def build_list(self) -> RecipeList:
        recipes = await RecipeCRUD(self.session).all(self.recipe_type)
        return RecipeList(
            data=[
                RecipeListItem(id=recipe.id, title=self.build_recipe_title(recipe), recipe_type=recipe.recipe_type)
                for recipe in recipes
            ],
            filters=RecipeListFilters(recipe_type=self.recipe_type),
        )

    @staticmethod
    def build_recipe_title(recipe: Recipe) -> str:
        return f"{recipe.title} ({RecipeType(recipe.recipe_type).value})"

    async def build_detail(self, recipe_id: UUID) -> RecipeDetail:
        try:
            recipe = await RecipeCRUD(self.session).get_by_id(recipe_id)
        except NoResultFound:
            raise RecipeDetailError()

        ingredients = self.build_ingredients(recipe.products)
        title = self.build_recipe_title(recipe)
        instructions = recipe.instruction
        nutrition_quality = self.build_nutrition_quality(recipe)
        return RecipeDetail(
            image=f"{app_config.media_folder}/{recipe.image}",
            text=f"{title} - на {self.person_count} чел."
            f"\n\nИнгредиенты:\n{ingredients}\n\n"
            f"Рецепт:\n{instructions}\n\n{nutrition_quality}",
        )

    def build_ingredients(self, ingredients: list[RecipeProduct]) -> str:
        measure_map = {
            MeasurementUnit.GRAM: "гр",
            MeasurementUnit.MILLILITER: "мл",
            MeasurementUnit.PIECE: "шт",
        }
        ingredients_lines = []
        for ingredient in ingredients:
            if ingredient.product.measurement_unit == MeasurementUnit.TO_TASTE:
                ingredients_lines.append(f"{ingredient.product.title}: по вкусу")
            else:
                ingredients_lines.append(
                    f"{ingredient.product.title}: "
                    f"{self._person_multiplier(ingredient.count)} "
                    f"{measure_map[ingredient.product.measurement_unit]}"
                )
        return "\n".join(ingredients_lines)

    def build_nutrition_quality(self, recipe: Recipe) -> str:
        return (
            f"КБЖУ: {self._person_multiplier(recipe.calories)}/"
            f"{self._person_multiplier(recipe.proteins)}"
            f"/{self._person_multiplier(recipe.fat)}"
            f"/{self._person_multiplier(recipe.carbohydrates)}"
        )

    def _person_multiplier(self, value: int) -> int:
        return value * self.person_count
