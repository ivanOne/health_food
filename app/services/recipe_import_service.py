import math

from crud.product_crud import ProductCRUD
from crud.recipe_crud import RecipeCRUD
from dto.recipe_import import ImportedRecipe
from models.recipe import MeasurementUnit, RecipeType
from services.base_service import BaseService


class RecipeImportService(BaseService):
    async def save(self, data: ImportedRecipe) -> None:
        product_cache = {}
        product_crud = ProductCRUD(self.session)
        recipe_crud = RecipeCRUD(self.session)

        recipe_products = list()
        for product in data.ingredients:
            product_name = product.name
            count = product.count

            product_obj = product_cache.get(product_name)
            if product_obj is None:
                product_obj = await product_crud.get_or_create(product_name, MeasurementUnit(product.measure_type))
                product_cache[product_name] = product_obj

            recipe_products.append((product_obj, count))

        recipe_data = {
            "id": data.id,
            "recipe_type": RecipeType(data.recipe_type),
            "title": data.name,
            "instruction": data.instructions,
            "image": data.image,
            "calories": data.nutrition_quality.calories,
            "proteins": data.nutrition_quality.proteins,
            "fat": data.nutrition_quality.fat,
            "carbohydrates": data.nutrition_quality.carbohydrates,
        }

        recipe_db = await recipe_crud.get_or_create(**recipe_data)
        await recipe_crud.add_products(recipe_db, recipe_products)


def divide_by_two(value: int) -> int:
    return math.ceil(value * 0.5)


def parse_products(products: str) -> list[dict]:
    result = []
    types_map = {
        MeasurementUnit.GRAM: "гр",
        MeasurementUnit.MILLILITER: "мл",
        MeasurementUnit.PIECE: "шт",
        MeasurementUnit.TO_TASTE: "вкусу",
    }
    for line in products.split("\n"):
        if line:
            name_and_count = line.split("-")
            name = name_and_count[0].strip().capitalize()
            try:
                cnt = name_and_count[1].strip().replace(".", "").lower()
            except IndexError:
                cnt = "по вкусу"

            for measure_type, detect in types_map.items():
                if detect in cnt:
                    result.append(
                        {
                            "name": name,
                            "count": (
                                divide_by_two(int(cnt.split(" ")[0]))
                                if measure_type != MeasurementUnit.TO_TASTE
                                else None
                            ),
                            "measure_type": measure_type.value,
                        }
                    )
    return result


def parse_nutrition_quality(value: str) -> dict:
    data = {
        "calories": 0,
        "proteins": 0,
        "fat": 0,
        "carbohydrates": 0,
    }
    if "/" in value:
        result = value.split("/")
        data["calories"] = divide_by_two(int(result[0].strip()))
        data["proteins"] = divide_by_two(int(result[1].strip()))
        data["fat"] = divide_by_two(int(result[2].strip()))
        data["carbohydrates"] = divide_by_two(int(result[3].strip()))
    else:
        for line in value.split("\n"):
            nutrition_keys_map = {
                "КК": "calories",
                "ББ": "proteins",
                "ЖЖ": "fat",
                "УУ": "carbohydrates",
            }
            for prefix, key in nutrition_keys_map.items():
                if prefix in line:
                    count = int(line.split("::")[-1].strip()[::2])
                    data[key] = divide_by_two(count)
    return data
