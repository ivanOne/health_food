from sqlalchemy.exc import NoResultFound

from crud.user_crud import UserCRUD
from models.user import User
from services.base_service import BaseService


class UserService(BaseService):
    async def create_new_user(self, telegram_id: int, person_count: int) -> None:
        await UserCRUD(self.session).create(User(telegram_user_id=telegram_id, person_count=person_count))

    async def get_person_count(self, telegram_id: int) -> int:
        try:
            user = await UserCRUD(self.session).get_by_telegram_id(telegram_id)
            return user.person_count
        except NoResultFound:
            return 1

    async def create_or_update_user(self, telegram_id: int, person_count: int) -> None:
        crud = UserCRUD(self.session)
        try:
            user = await crud.get_by_telegram_id(telegram_id)
            await crud.update(user, person_count)
        except NoResultFound:
            await self.create_new_user(telegram_id, person_count)
