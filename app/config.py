from pydantic_settings import BaseSettings


class AppConfig(BaseSettings):
    postgres_url: str
    redis_url: str
    admin_bot_token: str
    media_folder: str
    allowed_id: set[int]


app_config = AppConfig()
