import dataclasses
from uuid import UUID

from aiogram.filters.callback_data import CallbackData

from models.recipe import RecipeType


class RecipeTypeSelectCallback(CallbackData, prefix="recipe_import_type_choice"):
    recipe_type: RecipeType


@dataclasses.dataclass
class ImportedIngredient:
    name: str
    measure_type: str
    count: int | None = None


@dataclasses.dataclass
class ImportedNutritionQuality:
    calories: int
    proteins: int
    fat: int
    carbohydrates: int


@dataclasses.dataclass
class ImportedRecipe:
    id: UUID
    name: str
    ingredients: list[ImportedIngredient]
    instructions: str
    recipe_type: str
    nutrition_quality: ImportedNutritionQuality
    image: str
