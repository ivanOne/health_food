import dataclasses
from enum import Enum
from uuid import UUID

from aiogram.filters.callback_data import CallbackData

from models.recipe import RecipeType


@dataclasses.dataclass
class RecipeListItem:
    id: str
    title: str
    recipe_type: RecipeType


@dataclasses.dataclass
class RecipeListFilters:
    recipe_type: RecipeType | None


@dataclasses.dataclass
class RecipeList:
    data: list[RecipeListItem]
    filters: RecipeListFilters


@dataclasses.dataclass
class RecipeDetail:
    image: str
    text: str


class RecipeListView(str, Enum):
    LIST = "list"
    LIST_FILTER = "list_filter"
    DETAIL = "detail"
    SELECT_FILTER = "select_filter"


class RecipeListCallback(CallbackData, prefix="recipe_list"):
    action: RecipeListView
    recipe_id: UUID | None = None
    selected_filter: RecipeType | None = None
