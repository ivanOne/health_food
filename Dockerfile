FROM python:3.11-slim

ENV APP_ROOT=/app

RUN apt update && apt install -y \
    gcc \
    make \
    libpq-dev

COPY app $APP_ROOT/app

WORKDIR $APP_ROOT/app

RUN pip install -U pip && \
    pip install poetry

RUN poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-ansi
