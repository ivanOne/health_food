# Бот - сборник рецептов

## Запуск приложения
```
cp .env.example .env
docker compose up
```

## Миграции
### Создание миграций
```
docker compose run --rm bot alembic revision --autogenerate -m "comment"
docker compose run --rm bot alembic upgrade head
```
### Фиксация миграций
```
docker compose run --rm bot alembic upgrade head
```
### Откат миграций
```
docker compose run --rm bot alembic downgrade base|hash
```

## Линтеры
```
docker compose run --rm bot make lint
```